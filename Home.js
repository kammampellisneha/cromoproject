import './App.css';
import axios from 'axios';
import React, { useState,useEffect } from 'react';
import Cards from './Components/Cards';
import { Carousel} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


function Home(args) {
  const [data,setData] = useState([]); 
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3002/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);
  return (
    <div className='bg-black mt-0'>
      <div>
      <Carousel className='fixed '>
    <Carousel.Item >
      <img src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1701671977/Croma%20Assets/CMS/Homepage%20Banners/01_Homepage%20Bugs%20Daily/Income-ing%20Payday%20%7C%20Creatives%20%7C%201%20to%205%20Dec%202023/HP_Income-ingPayDay_04Dec2023_tzlwvd.png?tr=w-2048"  style={{width:'100%', height:'600px'}}/>
      <Carousel.Caption className='text-center fw-bolder'>
    
              </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
    <img src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1701071839/Croma%20Assets/CMS/LP%20Page%20Banners/2023/HP%20Top%20Rotating%20Deals/November/28%20Nov/HP_VivoT2_28Nov2023_y47iml.jpg?tr=w-2048" style={{width:'100%', height:'600px'}}/>
      <Carousel.Caption>
        
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
    <img src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1698065105/Croma%20Assets/CMS/LP%20Page%20Banners/2023/HP%20pcp/JBL%20Deals/HP%20Rotating/HP_JBL-TWS_21oct2023_bn87mw.png?tr=w-2048" style={{width:'100%',height:'600px'}}/>
      <Carousel.Caption>
        
      </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    <br/>
    <br/>
    </div>
    
    <div>
          <h3 className='text-info text-center text-white fs-10'>Deals of the Day</h3><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            <Cards data={e}/>
          ))} 
        </div>  
        </div>
        </div>
    </div>
  );
}

export default Home;