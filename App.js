import "./App.css";
import axios from "axios";
import React, { useState, useEffect } from "react";
import {
  NavLink,
  Navbar,
  Nav,
  Form,
  Container,
  Row,
  Col,
  NavDropdown,
} from "react-bootstrap";
import {Link } from 'react-router-dom';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./Components/Login.js";
import Home from "./Home";
import Mobiles from "./Components/Mobiles";
import Contact from "./Components/Contact";
import About from "./Components/About";
import ContactUS from "./Components/ContactUS";
import Refrigetors from "./Components/Refrigetors";



function App(args) {
  return (
    
    <div className="bg-color">
      <Navbar expand="lg" className="bg-black text-white gap-5 fixed-top">
        <Container>
         
          <Navbar.Brand>
            {" "}
            <img
              src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1637759004/Croma%20Assets/CMS/Category%20icon/Final%20icon/Croma_Logo_acrkvn.svg"
              alt="khan"
              style={{ width: "200px" }}
            />{" "}
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="basic-navbar-nav"
            style={{ color: "white" }}
          />
          <Navbar.Collapse id="basic-navbar-nav">
            <div className="d-flex justify-content-around">
              <div className="d-flex justify-content-around">
                <Nav className="me-auto bs-light text-white mx-5 gap-30">
                  <Nav.Link href="/" className=" text-white margin-left:5px">
                    HOME
                  </Nav.Link>
                  <Nav.Link href="/Mobiles" className=" text-white">
                   MOBILES 
                  </Nav.Link> 
                  <Nav.Link href="/Refrigetors" className=" text-white">
                   REFRIGETORS 
                  </Nav.Link> 
                  <Nav.Link href="/About" className=" text-white">
                    ABOUT
                  </Nav.Link>
                  <Nav.Link href="/Contact" className=" text-white">
                    CONTACT
                  </Nav.Link>
                </Nav>
              </div>
            </div>
            
          </Navbar.Collapse>
          <Form inline>
            <Row>
              {/* <Col xs="auto">
          <Form.Control
            type="text"
            placeholder="Search"
            className=" mr-sm-2"
          />
        </Col> */}
              <Col xs="auto"></Col>
            </Row>
          </Form>
          <Nav className="ml-auto flex-column display:flex" navbar>
            <NavLink>
              <Login />
            </NavLink>
          </Nav>
        </Container>
      </Navbar>
      <br />
      <br />
      <br />
      
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Mobiles" element={<Mobiles/>} /> 
            <Route path="/Refrigetors" element={<Refrigetors/>}/>
            <Route path="/About" element={<About />} />
            <Route path="/Contact" element={<ContactUS />} />
          </Routes>
        </BrowserRouter>
      </div><br/><br/>
      <div className='footer text-center text-black bg-white'>
      

    {/* <!-- Section: Form --> */}
    <section class="">
      <form action="">
        {/* <!--Grid row--> */}
        <div class="row d-flex justify-content-center">
          {/* <!--Grid column--> */}
          <div class="col-auto">
            <p class="pt-2">
              {/* <strong>Sign up for our newsletter</strong> */}
            </p>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-md-5 col-12">
            {/* <!-- Email input --> */}
            <div data-mdb-input-init class="form-outline mb-4">
              <input type="email" id="form5Example24" class="form-control" />
              <label class="form-label" for="form5Example24" >Email address</label>
             
            </div>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-auto">
            {/* <!-- Submit button --> */}
            <button data-mdb-ripple-init type="submit" class="btn btn-outline-black  mb-4" >
        
            </button>
          </div>
          {/* <!--Grid column--> */}
        </div>
        {/* <!--Grid row--> */}
      </form>
    </section>
          <p> © Copyright 2023 Croma. All rights reserved</p>
          <p></p>
        </div>
    </div>
  );
}

export default App;

