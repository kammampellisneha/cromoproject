
import React from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import { Navbar,NavbarBrand ,CardGroup} from 'reactstrap';
import Card from 'react-bootstrap/Card';
import Cards from './Cards';
import { useState,useEffect } from 'react'
import axios from 'axios';
function Refrigetors() {
  const [data,setData] = useState([]); 
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3002/fetch3")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);
    return (
    <div >
        {/* <div id="carouselExample" class="carousel slide mt-5"> */}
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1673078319/Croma%20Assets/CMS/CAtegory/Refrigerator%20-%20C47/UPDATED%20DESIGN/desktop/main%20banners/PCP_ref_6jan2023_wfmznm.png?tr=w-2048" height={450}  alt="..."></img>
        
          
            {/* <button onClick={()=>{navigate('/')}}>Back To Home Page</button>  */}
        </div>
      {/* </div> */}
    </div>
    <div className='bg-black mt-0'>
     <h4 className='text-info text-center text-white fs-10'>Latest Refrigerators For You</h4><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            <Cards data={e}/>
          ))} 
        </div>  
        </div>
        </div>
</div>     
  )
}
export default Refrigetors;