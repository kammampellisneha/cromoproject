import Cards from './Cards';
import React from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import { Navbar,NavbarBrand ,CardGroup} from 'reactstrap';
import Card from 'react-bootstrap/Card';
import { useState,useEffect } from 'react'
import axios from 'axios';
function Mobiles() {
  const [data,setData] = useState([]); 
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3002/fetch2")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);
  return (
    <div className='bg-black mt-0'>
        {/* <div id="carouselExample" class="carousel slide mt-5"> */}
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="https://media-ik.croma.com/prod/https://media.croma.com/image/upload/v1684242615/Croma%20Assets/CMS/CAtegory/Mobile%20phone%20-%20C10/16-05-23/Desktop/Main%20Banner/D_main-banner_hat0zq.png?tr=w-2048" height={500}  alt="..."></img>
        
          
            {/* <button onClick={()=>{navigate('/')}}>Back To Home Page</button>  */}
        {/* </div> */}
      </div>
    </div>
            <h5 className='text-info text-center text-white fs-10'>Latest Mobile Phones For You</h5><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            <Cards data={e}/>
          ))} 
        </div>  
        </div>
        </div>
       
  )
}
export default Mobiles;