import React, { useState } from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, Label, Alert } from 'reactstrap';
import Register from './Register';


function Login(args) {
  const [modal, setModal] = useState(false);

  const toggle = (e) => {
    e.preventDefault()
    setModal(!modal);
  }
  const [formdata, setFormdata] = useState({
    email: '',
    password: ''
  })
  const handleInput = (e) => {
    const { name, value } = e.target
    setFormdata({
      ...formdata,
      [name]: value
    })
  }
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await
        axios.get(`http://localhost:3002/login/${formdata.email}/${formdata.password}`)
      console.log(response)
    } catch (err) {
      throw err
    }
    alert("login successful")
  }
  console.log(formdata)
  return (
    <div>
      <Button color="danger" onClick={toggle} >
        Login
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <>

            <Form onSubmit={handleSubmit}>
              <FormGroup floating>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="Email"
                  type="email"
                  value={formdata.email}
                  onChange={handleInput}
                  required
                />
                <Label for="exampleEmail">
                  Email
                </Label>
              </FormGroup>
              {' '}
              <FormGroup floating>
                <Input
                  id="examplePassword"
                  name="password"
                  placeholder="Password"
                  type="password"
                  value={formdata.password}
                  onChange={handleInput}
                  required
                />
                <Label for="examplePassword">
                  Password
                </Label>
              </FormGroup>
              {' '}
              <Button type='submit'>
                Submit
              </Button>
              <Button><Register /></Button>
            </Form>
          </>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>

    </div>
  );
}


export default Login;